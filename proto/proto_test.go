package main

import (
	"bytes"
	"fmt"
	"testing"
)

func TestProto(t *testing.T) {
	tests := []struct {
		name       string
		proto      *Proto
		expectFail bool
	}{
		{
			name: "Basic test with hello",
			proto: &Proto{
				Header: Header{Cmd: 1, Sz: 5},
				Body:   Body{Data: []byte("hello")},
			},
		},
		{
			name: "Empty body",
			proto: &Proto{
				Header: Header{Cmd: 2, Sz: 0},
				Body:   Body{Data: []byte{}},
			},
		},
		{
			name: "Short body",
			proto: &Proto{
				Header: Header{Cmd: 3, Sz: 3},
				Body:   Body{Data: []byte("abc")},
			},
		},
		{
			name: "Long body",
			proto: &Proto{
				Header: Header{Cmd: 4, Sz: 15},
				Body:   Body{Data: []byte("thisisalongbody")},
			},
		},
		{
			name: "Single byte body",
			proto: &Proto{
				Header: Header{Cmd: 5, Sz: 1},
				Body:   Body{Data: []byte("x")},
			},
		},
		{
			name: "Large body",
			proto: &Proto{
				Header: Header{Cmd: 6, Sz: 1024},
				Body:   Body{Data: make([]byte, 1024)},
			},
		},
		{
			name: "Mixed content body",
			proto: &Proto{
				Header: Header{Cmd: 7, Sz: 9},
				Body:   Body{Data: []byte("123abcDEF")},
			},
		},
		{
			name: "Zero command",
			proto: &Proto{
				Header: Header{Cmd: 0, Sz: 4},
				Body:   Body{Data: []byte("test")},
			},
		},
		{
			name: "Max uint8 command",
			proto: &Proto{
				Header: Header{Cmd: 255, Sz: 4},
				Body:   Body{Data: []byte("maxC")},
			},
		},
	}

	for idx, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var buf bytes.Buffer

			err := tt.proto.Write(&buf)
			if err != nil {
				t.Fatalf("failed to write proto: %v", err)
			}

			readProto := &Proto{}
			err = readProto.Read(&buf)
			if err != nil {
				t.Fatalf("failed to read proto: %v", err)
			}

			if tt.proto.Header.Cmd != readProto.Header.Cmd {
				t.Errorf("expected cmd %v, got %v, test %v", tt.proto.Header.Cmd, readProto.Header.Cmd, tt.name)
			} else {
				fmt.Printf("ok test #%v, name %v\n", idx, tt.name)
			}
			if tt.proto.Header.Sz != readProto.Header.Sz {
				t.Errorf("expected size %v, got %v, test %v", tt.proto.Header.Sz, readProto.Header.Sz, tt.name)
			} else {
				fmt.Printf("ok test #%v, name %v\n", idx, tt.name)
			}

			if !bytes.Equal(tt.proto.Body.Data, readProto.Body.Data) {
				t.Errorf("expected data %v, got %v, test %v", tt.proto.Body.Data, readProto.Body.Data, tt.name)
			} else {
				fmt.Printf("ok test #%v, name %v\n", idx, tt.name)
			}
		})
	}
}
