package main

import (
	"encoding/binary"
	"io"
)

// header - стуктура содержащая поля заголовка пакета
type Header struct {
	Cmd uint8  // команда, которая передаётся // пример 1, 2, 3...
	Sz  uint32 // размер пакета, который передaётся
}

type Body struct {
	Data []byte // аналог payload, информация, которая передаётся в протоколе
}

// структура нашего протокола
type Proto struct {
	Header Header
	Body   Body
}

func (p *Proto) Write(w io.Writer) error {
	if err := binary.Write(w, binary.BigEndian, p.Header.Cmd); err != nil {
		return err
	}
	if err := binary.Write(w, binary.BigEndian, p.Header.Sz); err != nil {
		return err
	}
	if _, err := w.Write(p.Body.Data); err != nil {
		return err
	}
	return nil
}

// Read реализует интерфейс io.Reader
func (p *Proto) Read(r io.Reader) error {
	if err := binary.Read(r, binary.BigEndian, &p.Header.Cmd); err != nil {
		return err
	}
	if err := binary.Read(r, binary.BigEndian, &p.Header.Sz); err != nil {
		return err
	}
	p.Body.Data = make([]byte, p.Header.Sz)
	if _, err := io.ReadFull(r, p.Body.Data); err != nil {
		return err
	}
	return nil
}
